#!/usr/bin/env python

"""Submit meta data and release files into transparency log

@contact: Debian FTPMaster <ftpmaster@debian.org>
@copyright: 2019, Benjamin Hof <hof@in.tum.de>
@license: GNU General Public License version 2 or later

"""

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

################################################################################

import apt_pkg
import os
import os.path
import shutil
import socket
import sys

from daklib import daklog
from daklib.dbconn import *
from daklib.config import Config
import daklib.strans

################################################################################

Options = None
Logger = None

################################################################################


def usage(exit_code=0):
    print("""Usage: dak strans-submission OPTIONS
Request transparency log submission of archive meta data and release files,
include proofs into the archive

  -h, --help                 show this help and exit""")

    sys.exit(exit_code)

################################################################################


def path_list(base, filenames):
    """Generate list of paths to regular files"""
    paths = []
    for entry in filenames:
        path = os.path.join(base, entry)
        if not os.path.isfile(path) or os.path.islink(path):
            continue
        paths.append(path)
    return paths


def submit_meta(suites):
    """Request submission of meta data files into transparency log"""
    cnf = Config()
    paths = []
    for suite in suites:
        Logger.log(['Processing meta data for Suite: %s' % (suite.suite_name)])
        for dirpath, _, filenames in os.walk(suite.path):
            paths += path_list(dirpath, filenames)

    daklib.strans.submit(paths, cnf['Strans-Submission::Submit'], "suites")


def submit_releases(archive):
    """Request submission of release files into transparency log"""
    cnf = Config()
    paths = [archive.archive_name] # first, send the name of the archive
    zzz = os.path.join(archive.path, 'zzz-dists')
    for dirpath, _, filenames in os.walk(zzz):
        for filename in filenames:
            if filename != 'InRelease':
                continue
            paths.append(os.path.join(dirpath, filename))

    daklib.strans.submit(paths, cnf['Strans-Submission::Release'],
                         archive.archive_name)


def main():
    global Logger

    cnf = Config()

    for i in ["Help", "Suite"]:
        key = "Strans-Submission::Options::%s" % i
        if key not in cnf:
            cnf[key] = ""

    Arguments = [('h', "help", "Strans-Submission::Options::Help"),
                 ('a', 'archive', 'Strans-Submission::Options::Archive', 'HasArg'),
                 ('s', "suite", "Strans-Submission::Options::Suite")]

    suite_names = apt_pkg.parse_commandline(cnf.Cnf, Arguments, sys.argv)
    Options = cnf.subtree("Strans-Submission::Options")

    if Options["Help"]:
        usage()

    Logger = daklog.Logger('strans-submission')

    session = DBConn().session()

    if Options["Suite"]:
        suites = []
        for s in suite_names:
            suite = get_suite(s.lower(), session)
            if suite:
                suites.append(suite)
            else:
                print("cannot find suite %s" % s)
                Logger.log(['cannot find suite %s' % s])
    else:
        query = session.query(Suite).filter(Suite.untouchable == False)  # noqa:E712
        if 'Archive' in Options:
            archive_names = utils.split_args(Options['Archive'])
            query = query.join(Suite.archive).filter(Archive.archive_name.in_(archive_names))
        suites = query.all()

    submit_meta(suites)

    # deduplicate archives
    archives = {suite.archive.path: suite.archive for suite in suites}
    for archive in archives.values():
        submit_releases(archive)

        # update proofs in archive directory
        src = os.path.join(cnf['Strans-Submission::Proofs'],
                           archive.archive_name)
        dst = os.path.join(archive.path, 'proofs')
        shutil.rmtree(dst) # get rid of old files
        shutil.copytree(src, dst)

    Logger.close()

#######################################################################################


if __name__ == '__main__':
    main()

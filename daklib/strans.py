"""Software transparency log submission

@contact: Debian FTP Master <ftpmaster@debian.org>
@copyright: 2019  Benjamin Hof <hof@in.tum.de>
@license: GNU General Public License version 2 or later
"""

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import socket

from daklib.config import Config
from daklib.dak_exceptions import StransError
from daklib.daklog import Logger


def submit_plain(paths, address):
    """
    Take a list of paths and request their submission into the software
    transparency log.
    """
    cnf = Config()
    sock = socket.socket(socket.AF_UNIX)
    sock.connect(address)

    for path in paths:
        sock.sendall(path + '\n')
    sock.shutdown(socket.SHUT_WR) # tell the other side we're done sending

    timeout = 30.0 if cnf['Strans-Submission::Enforced'] == 'false' else 300.0
    sock.settimeout(timeout) # large submissions may take a while
    answer = sock.recv(4096)
    sock.close()
    if answer != b'ok':
        raise StransError("Strans-Submission failed")


def submit(paths, address, log_name):
    """
    Take a list of paths and request their submission into the software
    transparency log.  On errors, create a log message and, if need be,
    suppress exceptions.
    """
    log = Logger()
    try:
        submit_plain(paths, address)
    except Exception as e:
        if cnf['Strans-Submission::Enforced'] == 'false':
            Logger.log([log_name,
                        "Error during strans submission: {0}".format(e)])
        else:
            raise

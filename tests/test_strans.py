#! /usr/bin/python
#
# Copyright (C) 2019, Benjamin Hof <hof@in.tum.de>
# License: GPL-2+
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
import sys
if sys.version_info < (3, 3):
    import mock
else:
    from unittest import mock

from base_test import DakTestCase
from dak.process_upload import submittable_files, strans_submit
from dak.strans_submission import path_list, submit_meta, submit_releases
from daklib.dak_exceptions import StransError
from daklib.strans import submit, submit_plain


class StransUploadTest(DakTestCase):
    paths = ['a/b', 'b/c']
    filename = 'source_1.0-1_amd64.changes'
    address = 'sock'

    def setUp(self):
        self.upload = mock.MagicMock()
        filename = mock.PropertyMock(return_value=self.filename)
        directory = mock.PropertyMock(return_value='a/b')
        type(self.upload.changes).filename = filename
        type(self.upload).directory = directory

    # submittable_files
    def test_submittable_files(self):
        paths = submittable_files(self.upload)
        self.assertEqual(paths, ['a/b/source_1.0-1_amd64.changes'])

    def test_submittable_files_empty(self):
        upload = mock.MagicMock()
        paths = submittable_files(upload)
        self.assertEqual(paths, [])

    # strans_submit
    @mock.patch('dak.process_upload.Logger')
    @mock.patch('dak.process_upload.submittable_files')
    @mock.patch('dak.process_upload.daklib.strans.submit')
    def test_strans_submit(self, mocksubmit, mocksubmittable, mocklog):
        mocksubmittable.return_value = self.paths
        strans_submit(self.upload)
        mocksubmit.assert_called_once_with(self.paths, "/run/strans/submit",
                                           self.filename)

    # submit_plain
    @mock.patch('daklib.strans.socket.socket')
    def test_submit_plain(self, mocksock):
        mocksock.return_value.recv.return_value = b'ok'
        submit_plain(self.paths, self.address)
        from mock import call
        calls = [call(1),
                 call().connect(self.address),
                 call().sendall(self.paths[0] + '\n'),
                 call().sendall(self.paths[1] + '\n'),
                 call().shutdown(1),
                 call().settimeout(300.0),
                 call().recv(4096),
                 call().close(),
                ]
        self.assertEqual(mocksock.mock_calls, calls)

    @mock.patch('daklib.strans.socket.socket')
    def test_submit_plain_bad_reply(self, mocksock):
        mocksock.return_value.recv.return_value = b'bad'
        with self.assertRaises(StransError):
            submit_plain(self.paths, self.address)

    # submit
    @mock.patch('daklib.strans.Logger')
    @mock.patch('daklib.strans.submit_plain')
    def test_submit(self, mocksubmit, mocklog):
        submit(self.paths, self.address, 'testlog')

    # path_list
    @mock.patch('dak.strans_submission.os.path.islink')
    @mock.patch('dak.strans_submission.os.path.isfile')
    def test_path_list(self, mockfile, mocklink):
        mocklink.return_value = False
        result = path_list('/dir', self.paths)
        self.assertEqual(result, ['/dir/' + path for path in self.paths])

    # submit_meta
    @mock.patch('dak.strans_submission.Logger')
    @mock.patch('dak.strans_submission.os.walk')
    @mock.patch('dak.strans_submission.path_list')
    @mock.patch('dak.strans_submission.daklib.strans.submit')
    def test_submit_meta(self, mocksubmit, mockpath, mockwalk, mocklog):
        paths = ['/srv/suite/file.' + item for item in "xy"]
        mockpath.return_value = paths
        mockwalk.return_value = [('/srv/suite', None, ['file.x', 'file.y'])]

        suite = mock.MagicMock()
        submit_meta([suite])
        mocksubmit.assert_called_once_with(paths, "/run/strans/submit",
                                           "suites")

    # submit_releases
    @mock.patch('dak.strans_submission.os.walk')
    @mock.patch('dak.strans_submission.daklib.strans.submit')
    def test_submit_releases(self, mocksubmit, mockwalk):
        mockwalk.return_value = [('/archive', None, ['InRelease', 'Release'])]

        archive = mock.MagicMock()
        archive_name = mock.PropertyMock(return_value='stable')
        path = mock.PropertyMock(return_value='/archive')
        type(archive).archive_name = archive_name
        type(archive).path = path

        submit_releases(archive)
        mocksubmit.assert_called_once_with(['stable', '/archive/InRelease'],
                                           "/run/strans/release",
                                           "stable")


if __name__ == '__main__':
    unittest.main()
